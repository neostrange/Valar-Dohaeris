#
# This directory contains all data files that Lvg use.
#
- Build.txt: build information, such as date, SCRs, details
- config: contains Lvg configuration files
- HSqlDb: contains HSqlDb data base files
- misc:   contains files of flat file system
- ReadMe.txt: this is it
- rules:  contains rule files for persistent trie to use
- tables: contains files for Lvg database tables
